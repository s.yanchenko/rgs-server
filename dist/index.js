"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const insur_agree_json_1 = __importDefault(require("./mock/insur-agree.json"));
const calculation_json_1 = __importDefault(require("./mock/calculation.json"));
const cors_1 = __importDefault(require("cors"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const port = process.env.PORT;
app.use((0, cors_1.default)({
    origin: 'http://127.0.0.1:1555'
}));
app.get('/initialValues', (0, cors_1.default)(), (req, res) => {
    res.send(insur_agree_json_1.default);
});
app.post('/sendOnApprove', (0, cors_1.default)(), (req, res) => {
    insur_agree_json_1.default.status = "ProcessApproval";
    insur_agree_json_1.default.sendDate = new Date().getTime();
    res.status(200).send();
});
app.post('/sendDocument', (0, cors_1.default)(), (req, res) => {
    insur_agree_json_1.default.document = { type: "PDF", name: "Паспорт ТС", link: "https://link-upload-file.ru" };
    res.status(200).send();
});
app.get('/contract-terms', (0, cors_1.default)(), (req, res) => {
    res.send(calculation_json_1.default.contractTerms);
});
app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
