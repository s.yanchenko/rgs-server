import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import insurAgreeJson from "./mock/insur-agree.json";
import calculationsJson from "./mock/calculation.json";
import cors from "cors"

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.use(cors({
  origin: 'http://127.0.0.1:1555'
}));

app.get('/initialValues', cors(), (req: Request, res: Response) => {
  res.send(insurAgreeJson);
});

app.post('/sendOnApprove', cors(), (req: Request, res: Response) => {
  insurAgreeJson.status = "ProcessApproval"
  insurAgreeJson.sendDate = new Date().getTime()
  res.status(200).send();
});

app.post('/sendDocument', cors(), (req: Request, res: Response) => {
  insurAgreeJson.document = {type: "PDF", name: "Паспорт ТС", link: "https://link-upload-file.ru"}
  res.status(200).send();
});

app.get('/contract-terms', cors(), (req: Request, res: Response) => {
  res.send(calculationsJson.contractTerms);
});

app.get('/policyholder', cors(), (req: Request, res: Response) => {
  res.send(calculationsJson.policyholder);
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});